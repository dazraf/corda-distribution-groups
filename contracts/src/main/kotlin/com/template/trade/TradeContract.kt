package com.template.trade

import com.template.legal.LegalDocument
import net.corda.core.contracts.Command
import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

class TradeContract : Contract {
  companion object {
    // Used to identify our contract when building a transaction.
    val ID = TradeContract::class.qualifiedName!!
  }

  override fun verify(tx: LedgerTransaction) {
    val legalDoc = tx.referenceInputsOfType<LegalDocument>().singleOrNull() ?: error("that there is a legal doc reference")
    val trade = tx.outputsOfType<Trade>().singleOrNull() ?: error("that there is only one trade ")
    check(trade.legalDocumentPointer.pointer == legalDoc.linearId) { "that the legal doc is correctly referenced by the trade"}
  }

  // Used to indicate the transaction's intent.
  interface Commands : CommandData {
    fun create(trade: Trade) = Command(this, trade.participants.map { it.owningKey })
    object Execute : Commands, TypeOnlyCommandData()
  }
}
