package com.template.trade

import com.template.legal.LegalDocument
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.ContractState
import net.corda.core.contracts.LinearPointer
import net.corda.core.identity.AbstractParty

@BelongsToContract(TradeContract::class)
data class Trade(
  val legalDocumentPointer: LinearPointer<LegalDocument>,
  override val participants: List<AbstractParty>
) : ContractState
