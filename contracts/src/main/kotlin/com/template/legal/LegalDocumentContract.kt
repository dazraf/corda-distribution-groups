package com.template.legal

import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

class LegalDocumentContract : Contract {
  companion object {
    // Used to identify our contract when building a transaction.
    val ID = LegalDocumentContract::class.qualifiedName!!
  }

  // A transaction is valid if the verify() function of the contract of all the transaction's input and output states
  // does not throw an exception.
  override fun verify(tx: LedgerTransaction) {
    // Verification logic goes here.
  }

  // Used to indicate the transaction's intent.
  interface Commands : CommandData {
    fun create(legalDocument: LegalDocument) = Command(this, legalDocument.participants.map { it.owningKey })
    object Create : Commands, TypeOnlyCommandData()
    object Update : Commands, TypeOnlyCommandData()
  }
}
