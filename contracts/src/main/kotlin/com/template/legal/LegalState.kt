package com.template.legal

import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party

@BelongsToContract(LegalDocumentContract::class)
data class LegalDocument(
  val text: String,
  val issuer: Party,
  override val linearId: UniqueIdentifier = UniqueIdentifier("DOC")
) : LinearState {
  override val participants: List<AbstractParty>
    get() = listOf(issuer)
}
