This repo is for a test case of distribution groups that we need in our project. 

# Synopsis 

Parties: `A` and `B`.

1. `A` creates a `LegalDocument` (a `LinearState`) and also a distribution group that represents parties interested in notifications of change to this document.
2. `A` makes a `Trade` with `B` referencing `LegalDocument` as a `LinearPointer`. `B` successfully receives the document as a reference. 
3. `A` adds `B` to the distribution group for the document.
4. We then attempt to make a change to the `LegalDocument` and broadcast the change, in two different ways which both spectacularly fail.
    1. Notify the distribution group within the same transaction that updates the document
    2. Notify the distribution after updating the document
    
<del>These failure scenarios that are captured in [FlowTests](workflows/src/test/kotlin/com/template/FlowTests.kt)</del>

Happy to say both cases now work :-)