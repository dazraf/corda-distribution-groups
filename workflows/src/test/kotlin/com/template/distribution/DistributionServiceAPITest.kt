package com.template.distribution

import com.template.flows.legal.IssueLegalDocumentFlow
import com.template.legal.LegalDocument
import net.corda.core.internal.packageName
import net.corda.testing.common.internal.testNetworkParameters
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkParameters
import net.corda.testing.node.TestCordapp
import org.junit.After
import org.junit.Before

class DistributionServiceAPITest {
  private val cordappsForAllNodes = listOf(
    TestCordapp.findCordapp(LegalDocument::class.packageName),
    TestCordapp.findCordapp(IssueLegalDocumentFlow::class.packageName)
  )
  private val network = MockNetwork(MockNetworkParameters(
    cordappsForAllNodes = cordappsForAllNodes
    , networkParameters = testNetworkParameters(minimumPlatformVersion = 4)
//    ,threadPerNode = false
  ))
  private val a = network.createNode()
  private val b = network.createNode()
  private val notary = network.defaultNotaryIdentity

  @Before
  fun before() = network.startNodes()

  @After
  fun tearDown() = network.stopNodes()

}