package com.template

import com.template.distribution.flows.UpdateDistributionGroupFlow
import com.template.flows.legal.IssueLegalDocumentFlow
import com.template.flows.legal.UpdateLegalDocumentFlowV1
import com.template.flows.legal.UpdateLegalDocumentFlowV2
import com.template.flows.trade.ExecuteTradeFlow
import com.template.legal.LegalDocument
import com.template.trade.Trade
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.internal.packageName
import net.corda.core.node.services.queryBy
import net.corda.core.utilities.getOrThrow
import net.corda.testing.common.internal.testNetworkParameters
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkParameters
import net.corda.testing.node.StartedMockNode
import net.corda.testing.node.TestCordapp
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class FlowTests {
  private val cordappsForAllNodes = listOf(
    TestCordapp.findCordapp(LegalDocument::class.packageName),
    TestCordapp.findCordapp(IssueLegalDocumentFlow::class.packageName)
  )
  private val network = MockNetwork(MockNetworkParameters(
    cordappsForAllNodes = cordappsForAllNodes
    , networkParameters = testNetworkParameters(minimumPlatformVersion = 4)
//    ,threadPerNode = false
  ))
  private val a = network.createNode()
  private val b = network.createNode()
  private val notary = network.defaultNotaryIdentity

  @Before
  fun before() = network.startNodes()

  @After
  fun tearDown() = network.stopNodes()

  // ----------------------- KEY TESTS THAT FAIL ------------------------

  @Test
  fun `attempt to update distro group outside of the transaction`() {
    val uidLegalDoc = createLegalDocAndTrade(notary)

    // update legal doc
    val secondText = "thou shalt chill"
    a.execute(UpdateLegalDocumentFlowV1(uidLegalDoc, secondText, notary))

    // and update the distribution group independently
    a.execute(UpdateDistributionGroupFlow(uidLegalDoc))

    // check that node b knows about the update
    b.services.vaultService.queryBy<Trade>().states.single().state.data.legalDocumentPointer.resolve(b.services).state.data.apply {
      assertEquals(secondText, this.text, "that text of document referenced on node b matches the latest")
    }
  }

  @Test
  fun `attempt to update distro group within the transaction`() {
    val uidLegalDoc = createLegalDocAndTrade(notary)

    // update legal doc and update transactionally
    // THIS FAILS
    val secondText = "thou shalt chill"
    a.execute(UpdateLegalDocumentFlowV2(uidLegalDoc, secondText, notary))

    // check that node b knows about the update
    b.services.vaultService.queryBy<Trade>().states.single().state.data.legalDocumentPointer.resolve(b.services).state.data.apply {
      assertEquals(secondText, this.text, "that text of document referenced on node b matches the latest")
    }
  }

  // ----------------------- KEY TESTS THAT FAIL ------------------------

  private fun createLegalDocAndTrade(notary: Party): UniqueIdentifier {
    // create the legal doc
    val firstText = "thou shalt work"
    val uidLegalDoc = a.execute(IssueLegalDocumentFlow(firstText, notary))

    // do a trade against that legal doc
    a.execute(ExecuteTradeFlow(uidLegalDoc, b.info.legalIdentities.first(), notary))

    // check that node b knows about the legal doc
    b.services.vaultService.queryBy<Trade>().states.single().state.data.legalDocumentPointer.resolve(b.services).state.data.apply {
      assertEquals(firstText, this.text, "that text of document referenced on node b matches the initial")
    }
    return uidLegalDoc
  }

  private fun <T> StartedMockNode.execute(flow: FlowLogic<T>): T {
    val future = this.startFlow(flow)
    network.runNetwork()
    return future.getOrThrow()
  }
}

