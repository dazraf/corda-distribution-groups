package com.template.utilities

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.node.services.VaultService
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria

inline fun <reified T: LinearState> VaultService.getLinearState(uid: UniqueIdentifier): StateAndRef<T>? {
  val query = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(uid))
  return queryBy<T>(query).states.singleOrNull()
}

@Suspendable
inline fun <reified T: LinearState>VaultService.getLinearStates(uids: List<UniqueIdentifier>): List<StateAndRef<T>> {
  val query = QueryCriteria.LinearStateQueryCriteria(linearId = uids)
  return queryBy<T>(query).states
}
