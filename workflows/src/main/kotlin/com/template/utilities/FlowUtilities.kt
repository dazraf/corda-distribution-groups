package com.template.utilities

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@Suspendable
fun <T> FlowLogic<T>.execute(transactionBuilder: TransactionBuilder) : SignedTransaction {
  val pstx = serviceHub.signInitialTransaction(transactionBuilder)
  val counterparties = pstx.tx.outputs.flatMap { it.data.participants }.map { it as Party } - ourIdentity
  val sessions = counterparties.map { initiateFlow(it) }
  val finalStx = subFlow(CollectSignaturesFlow(pstx, sessions))
  return subFlow(FinalityFlow(finalStx, sessions))
}

@Suspendable
fun FlowLogic<*>.signAndFinality(otherSession: FlowSession, checkTransaction: (SignedTransaction) -> Unit = {}) : SignedTransaction {
  val stx = subFlow(object : SignTransactionFlow(otherSession) {
    override fun checkTransaction(stx: SignedTransaction) {
      checkTransaction(stx)
    }
  })
  return subFlow(ReceiveFinalityFlow(otherSession, stx.id))
}
