package com.template.distribution.impl

import co.paralleluniverse.fibers.Suspendable
import com.template.distribution.DistributionServiceAPI
import com.template.distribution.flows.UpdateDistributionGroupFlow
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.CordaService
import net.corda.core.serialization.SingletonSerializeAsToken
import net.corda.core.utilities.contextLogger
import net.corda.core.utilities.getOrThrow
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Path
import kotlin.reflect.KProperty1


@Suppress("unused") // top level service
@CordaService
class DistributionService(private val serviceHub: AppServiceHub): SingletonSerializeAsToken(), DistributionServiceAPI {
  companion object {
    private val logger = contextLogger()
  }

  init {
    logger.info("Distribution Service started")
  }

  @Suspendable
  override fun create(distributionId: UniqueIdentifier, description: String) {
    if (exists(distributionId)) {
      error("distribution already exists $distributionId")
    }
    serviceHub.withEntityManager {
      persist(Distribution(distributionId.id, description))
    }
  }

  @Suspendable
  override fun exists(distributionId: UniqueIdentifier) = getDistribution(distributionId) != null

  @Suspendable
  fun getDistribution(distributionId: UniqueIdentifier): Distribution? {
    return serviceHub.withEntityManager {
      val query: CriteriaQuery<Distribution> = criteriaBuilder.createQuery(Distribution::class.java)
      query.apply {
        val root = from(Distribution::class.java)
        where(criteriaBuilder.equal(root.get(Distribution::linearId), distributionId.id))
        select(root)
      }
      createQuery(query).resultList
    }.singleOrNull()
  }

  @Suspendable
  override fun addMember(distributionId: UniqueIdentifier, member: Party) : Boolean {
    return addMembers(distributionId to member).isNotEmpty()
  }

  @Suspendable
  override fun addMembers(vararg members: Pair<UniqueIdentifier, Party>): Set<Pair<UniqueIdentifier, Party>> {
    return addMembers(members.toSet())
  }

  @Suspendable
  override fun addMembers(members: Set<Pair<UniqueIdentifier, Party>>): Set<Pair<UniqueIdentifier, Party>> {
    val idAndParties = filterNewParties(members)
    if (idAndParties.isNotEmpty()) {
      addPartiesToDistributionList(idAndParties)
    }
    return idAndParties
  }

  @Suspendable
  override fun removeMembers(vararg members: Pair<UniqueIdentifier, Party>) {
    removeMembers(members.toSet())
  }

  @Suspendable
  override fun removeMembers(members:Set<Pair<UniqueIdentifier, Party>>) {
    removePartiesFromDistributionList(members)
  }

  @Suspendable
  override fun getMembers(distributionId: UniqueIdentifier): Set<Party> {
    if (!exists(distributionId)) error("distribution not found $distributionId")
    return serviceHub.withEntityManager {
      val query: CriteriaQuery<DistributionRecord> = criteriaBuilder.createQuery(DistributionRecord::class.java)
      query.apply {
        val root = from(DistributionRecord::class.java)
        where(criteriaBuilder.equal(root.get(DistributionRecord::linearId), distributionId.id))
        select(root)
      }
      createQuery(query).resultList.map { it.party }.toSet()
    }
  }

  @Suspendable
  override fun hasMember(distributionId: UniqueIdentifier, member: Party): Boolean {
    return getMemberRecord(distributionId, member) != null
  }

  @Suspendable
  fun getMemberRecord(distributionId: UniqueIdentifier, party: Party): DistributionRecord? {
    return serviceHub.withEntityManager {
      val query: CriteriaQuery<DistributionRecord> = criteriaBuilder.createQuery(DistributionRecord::class.java)
      query.apply {
        val root = from(DistributionRecord::class.java)
        val linearIdEq = criteriaBuilder.equal(root.get(DistributionRecord::linearId), distributionId.id)
        val partyEq = criteriaBuilder.equal(root.get(DistributionRecord::party), party)
        where(criteriaBuilder.and(linearIdEq, partyEq))
        select(root)
      }
      createQuery(query).resultList
    }.singleOrNull()
  }

  @Suspendable
  private fun filterNewParties(members: Set<Pair<UniqueIdentifier, Party>>): Set<Pair<UniqueIdentifier, Party>> {
    return members.filter { (uid, party) -> !hasMember(uid, party) }.toSet()
  }

  @Suspendable
  private fun addPartiesToDistributionList(idAndParties: Collection<Pair<UniqueIdentifier, Party>>) {
    serviceHub.withEntityManager {
      idAndParties.forEach { (uid, party) ->
        persist(DistributionRecord(uid.id, party))
      }
    }
  }

  @Suspendable
  private fun removePartiesFromDistributionList(idAndParties: Set<Pair<UniqueIdentifier, Party>>) {
    serviceHub.withEntityManager {
      // slow but easy TODO: optimise
      idAndParties
        .asSequence()
        .map { (uid, party) -> DistributionRecord(uid.id, party) }
        .forEach { remove(it) }
    }
  }

  @Suspendable
  fun sendLatestStateToParties(distributionId: UniqueIdentifier) {
    serviceHub.startFlow(UpdateDistributionGroupFlow(distributionId)).returnValue.getOrThrow()
  }

  private inline fun <reified X, reified T> Path<X>.get(property: KProperty1<X, T>): Path<T> {
    return this.get<T>(property.name)
  }
}
