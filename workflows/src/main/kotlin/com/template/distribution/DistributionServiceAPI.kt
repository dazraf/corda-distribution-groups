package com.template.distribution

import co.paralleluniverse.fibers.Suspendable
import com.template.distribution.flows.UpdateDistributionGroupFlow
import com.template.distribution.impl.DistributionService
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.node.ServiceHub

/**
 * Get the global distribution service
 */
@Suspendable
fun ServiceHub.distributionService() : DistributionServiceAPI {
  return cordaService(DistributionService::class.java)
}

/**
 * Update the distribution group with the latest version of a value
 */
@Suspendable
fun FlowLogic<*>.updateDistributionGroup(distributionId: UniqueIdentifier) {
  subFlow(UpdateDistributionGroupFlow(distributionId))
}

/**
 * Provide a means of creating simple distribution groups
 */
interface DistributionServiceAPI {
  /**
   * Create a distribution group with [distributionId] and [description]
   */
  @Suspendable
  fun create(distributionId: UniqueIdentifier, description: String)

  /**
   * True iff a distribution group with id [distributionId] exists
   */
  @Suspendable
  fun exists(distributionId: UniqueIdentifier): Boolean

  /**
   * Add a new member to a distribution list held locally.
   * @return true iff a new record was added.
   */
  @Suspendable
  fun addMember(distributionId: UniqueIdentifier, member: Party): Boolean

  /**
   * Add many members to many distributions.
   * @return set of actual [Pair<UniqueIdentifier, Party>] that were added
   */
  @Suspendable
  fun addMembers(vararg members: Pair<UniqueIdentifier, Party>): Set<Pair<UniqueIdentifier, Party>>

  /**
   * Add many members to many distributions.
   * @return set of actual [Pair<UniqueIdentifier, Party>] that were added
   */
  @Suspendable
  fun addMembers(members: Set<Pair<UniqueIdentifier, Party>>): Set<Pair<UniqueIdentifier, Party>>

  /**
   * Remove [members]
   */
  @Suspendable
  fun removeMembers(vararg members: Pair<UniqueIdentifier, Party>)

  /**
   * Remove [members]
   */
  @Suspendable
  fun removeMembers(members: Set<Pair<UniqueIdentifier, Party>>)

  /**
   * @return the set of members for distribution [distributionId]
   */
  @Suspendable
  fun getMembers(distributionId: UniqueIdentifier): Set<Party>

  /**
   * @return true iff distribution [distributionId] contains [member]
   */
  @Suspendable
  fun hasMember(distributionId: UniqueIdentifier, member: Party): Boolean

  /**
   * Update all members of distribution [distributionId] with the latest version of the [LinearState]
   * with the same id
   */
  @Suspendable
  fun updateDistributionGroup(flow: FlowLogic<*>, distributionId: UniqueIdentifier) {
    flow.subFlow(UpdateDistributionGroupFlow(distributionId))
  }
}
