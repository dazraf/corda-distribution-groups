package com.template.distribution.flows

import co.paralleluniverse.fibers.Suspendable
import com.template.distribution.distributionService
import com.template.utilities.getLinearState
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.StatesToRecord
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.contextLogger

/**
 * This flow is invoked to update reference data for a set of recipients
 */
@StartableByRPC
@StartableByService
@InitiatingFlow
open class UpdateDistributionGroupFlow(
  private val uid: UniqueIdentifier
) : FlowLogic<Unit>() {
  companion object {
    private val log = contextLogger()
  }
  @Suspendable
  override fun call() {
    val state = serviceHub.vaultService.getLinearState<LinearState>(uid)
      ?: error("could not find distribution data $uid")

    val tx = serviceHub.validatedTransactions.getTransaction(state.ref.txhash) ?: error("Can't find tx with specified hash.")

    serviceHub
      .distributionService().getMembers(uid)
      .apply { log.info("updating $size parties with the latest state $uid") }
      .forEach { send(it, tx) }
      .also { log.info("done update for $uid") }
  }

  @Suspendable
  private fun send(party: Party, tx: SignedTransaction) {
    val session = initiateFlow(party)
    subFlow(SendTransactionFlow(session, tx))
  }
}

/**
 * Received state updates from a distribution list maintainer
 * This class is an "inline" flow and should be invoked from a top level [InitiatedBy] flow
 */
@InitiatedBy(UpdateDistributionGroupFlow::class)
open class ReceiveUpdateDistributionGroupFlow(private val otherSession: FlowSession) : FlowLogic<Unit>() {
  companion object {
    private val log = contextLogger()
  }
  @Suspendable
  override fun call() {
    log.info("receiving update to distribution group")
    subFlow(ReceiveTransactionFlow(otherSession, statesToRecord = StatesToRecord.ALL_VISIBLE))
    log.info("finished receiving update to distribution group")
  }
}