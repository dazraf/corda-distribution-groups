package com.template.distribution.flows

import co.paralleluniverse.fibers.Suspendable
import com.template.distribution.distributionService
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.FlowSession
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import net.corda.core.utilities.unwrap

/**
 * This flow is executed by a member of one or more distribution group to ask the maintainer to add a set of respective
 * parties. It is intended to be used as an inline flow. The sender must be a member of all the distribution groups in
 * [additions]
 */
open class AddMemberToDistributionFlow(
  private val session: FlowSession,
  private val additions: Set<Pair<UniqueIdentifier, Party>>
) : FlowLogic<Boolean>() {
  @Suspendable
  override fun call() : Boolean{
    session.send(additions)
    return session.receive<Boolean>().unwrap { it }
  }
}

/**
 * Handler for the flow above. This flow
 */
open class AddMemberToDistributionHandlerFlow(private val otherSession: FlowSession) : FlowLogic<Unit>() {
  @Suspendable
  override fun call() {
    val distributionService = serviceHub.distributionService()
    val payload = otherSession.receive<AddMemberRequestPayload>().unwrap { payload ->
      requireThat {
        "requester matches the session party" using (otherSession.counterparty == payload.requester)
        "requester is member of all distribution lists being referred to" using (
            payload.additions.all { (uid, _) -> distributionService.hasMember(uid, payload.requester) }
          )
      }
      payload
    }
    val additions = distributionService.addMembers(payload.additions)
    otherSession.send(additions.isNotEmpty())
  }
}

@CordaSerializable
data class AddMemberRequestPayload(val requester: Party, val additions: Set<Pair<UniqueIdentifier, Party>>)