package com.template.flows.trade

import co.paralleluniverse.fibers.Suspendable
import com.template.distribution.distributionService
import com.template.utilities.execute
import com.template.utilities.signAndFinality
import com.template.trade.TradeContract
import com.template.legal.LegalDocument
import com.template.trade.Trade
import net.corda.core.contracts.LinearPointer
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
@InitiatingFlow
class ExecuteTradeFlow(private val uiLegalDocument: UniqueIdentifier, private val counterparty: Party, private val notary: Party) : FlowLogic<SignedTransaction>() {
  @Suspendable
  override fun call(): SignedTransaction {
    val legalDocPointer = LinearPointer(uiLegalDocument, LegalDocument::class.java)
    val txb = TransactionBuilder(notary).apply {
      val trade = Trade(legalDocPointer, listOf(ourIdentity, counterparty))
      addOutputState(trade, TradeContract.ID)
      addCommand(TradeContract.Commands.Execute.create(trade))
    }
    return execute(txb).also {
      serviceHub.distributionService().addMember(uiLegalDocument, counterparty)
    }
  }
}

@Suppress("unused") // responding flow
@InitiatedBy(ExecuteTradeFlow::class)
class ExecuteTradeRecipient(private val otherSession: FlowSession) : FlowLogic<SignedTransaction>() {
  @Suspendable
  override fun call(): SignedTransaction {
    return signAndFinality(otherSession)
  }
}
