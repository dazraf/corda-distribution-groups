package com.template.flows.legal

import co.paralleluniverse.fibers.Suspendable
import com.template.distribution.updateDistributionGroup
import com.template.legal.LegalDocument
import com.template.legal.LegalDocumentContract
import com.template.utilities.execute
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.StartableByRPC
import net.corda.core.identity.Party
import net.corda.core.node.services.queryBy
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.node.services.vault.QueryCriteria.LinearStateQueryCriteria as LinearStateQueryCriteria1

/**
 * This updates the legal doc and notifies the distribution group
 */
@StartableByRPC
class UpdateLegalDocumentFlowV2(
  private val uid: UniqueIdentifier,
  private val text: String,
  private val notary: Party
) : FlowLogic<UniqueIdentifier>() {
  @Suspendable
  override fun call(): UniqueIdentifier {
    val oldState = serviceHub.vaultService.queryBy<LegalDocument>(LinearStateQueryCriteria1(linearId = listOf(uid))).states.singleOrNull()
      ?: error("could not locate LegalDocument $uid")

    val newState = oldState.state.data.copy(text = text)
    val txb = TransactionBuilder(notary).apply {
      addInputState(oldState)
      addCommand(LegalDocumentContract.Commands.Update.create(newState))
      addOutputState(newState, LegalDocumentContract.ID)
    }
    execute(txb)
    updateDistributionGroup(newState.linearId)
    return newState.linearId
  }
}
