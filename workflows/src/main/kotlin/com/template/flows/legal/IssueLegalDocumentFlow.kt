package com.template.flows.legal

import com.template.distribution.distributionService
import com.template.utilities.execute
import com.template.legal.LegalDocumentContract
import com.template.legal.LegalDocument
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.StartableByRPC
import net.corda.core.identity.Party
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class IssueLegalDocumentFlow(private val text: String, private val notary: Party) : FlowLogic<UniqueIdentifier>() {
  override fun call(): UniqueIdentifier {
    val state = LegalDocument(text, ourIdentity)
    val stx = TransactionBuilder(notary).apply {
      addCommand(LegalDocumentContract.Commands.Create.create(state))
      addOutputState(state, LegalDocumentContract.ID)
    }
    execute(stx)
    serviceHub.distributionService().create(state.linearId, "legal doc")
    return state.linearId
  }
}
