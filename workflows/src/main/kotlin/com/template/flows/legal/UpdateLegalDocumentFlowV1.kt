package com.template.flows.legal

import co.paralleluniverse.fibers.Suspendable
import com.template.utilities.execute
import com.template.legal.LegalDocumentContract
import com.template.legal.LegalDocument
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.StartableByRPC
import net.corda.core.identity.Party
import net.corda.core.node.services.queryBy
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.node.services.vault.QueryCriteria.LinearStateQueryCriteria as LinearStateQueryCriteria1

/**
 * This updates the legal doc and does not notify existing members of the distribution group
 */
@StartableByRPC
class UpdateLegalDocumentFlowV1(
  private val uid: UniqueIdentifier,
  private val text: String,
  private val notary: Party
) : FlowLogic<UniqueIdentifier>() {
  @Suspendable
  override fun call(): UniqueIdentifier {
    val oldState = serviceHub.vaultService.queryBy<LegalDocument>(LinearStateQueryCriteria1(linearId = listOf(uid))).states.singleOrNull()
      ?: error("could not locate LegalDocument $uid")

    val newState = oldState.state.data.copy(text = text)
    val txb = TransactionBuilder(notary).apply {
      addInputState(oldState)
      addCommand(LegalDocumentContract.Commands.Update.create(newState))
      addOutputState(newState, LegalDocumentContract.ID)
    }
    execute(txb)
    return newState.linearId
  }
}
